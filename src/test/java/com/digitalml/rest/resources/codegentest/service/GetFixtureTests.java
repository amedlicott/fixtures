package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Fixtures.FixturesServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.FixturesService.GetFixtureInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.FixturesService.GetFixtureReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetFixtureTests {

	@Test
	public void testOperationGetFixtureBasicMapping()  {
		FixturesServiceDefaultImpl serviceDefaultImpl = new FixturesServiceDefaultImpl();
		GetFixtureInputParametersDTO inputs = new GetFixtureInputParametersDTO();
		inputs.setRequest(org.apache.commons.lang3.StringUtils.EMPTY);
		GetFixtureReturnDTO returnValue = serviceDefaultImpl.getFixture(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}